package se.elsgard.game.entity;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Rectangle;

import java.awt.*;

public class Entity {
    protected int width;
    protected int height;
    protected int x;
    protected int y;
    protected Rectangle rectangle;
    protected Texture texture;

    public Entity(int width, int height, int x , int y, Texture texture) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
        this.rectangle = new Rectangle(x, y, width, height);
        this.texture = texture;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }
}
