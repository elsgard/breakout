package se.elsgard.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import se.elsgard.game.BreakoutGame;
import se.elsgard.game.Log;

import java.awt.*;
import java.util.Date;

public class Ball extends Entity{
    private static final int MILLIS_UNTIL_COLLIDE = 100;
    private static final int MAX_BOUNCE_ANGLE = 75;
    private boolean isAttached;
    private boolean shouldCollide = false;
    private Date releasedAt;
    private float dX;
    private float dY;
    private float speed;
    private BreakoutGame game;
    public Ball(int width, int height, int x, int y, Texture texture, BreakoutGame game) {
        super(width, height, x, y, texture);
        this.game = game;
    }

    public boolean getIsAttached() {
        return isAttached;
    }

    public void setAttached(boolean attached)
    {
        isAttached = attached;
    }

    public boolean getShouldCollide() {
        return shouldCollide;
    }

    public void move(){
        dY = 0.7f;
        dX = 0.3f;
        speed = 500.0f;
        isAttached = false;
        releasedAt = new Date();
    }

    public void update(float delta){
        if(!shouldCollide && new Date().getTime() - releasedAt.getTime() >= MILLIS_UNTIL_COLLIDE)
            shouldCollide = true;

        x = x + (int)(dX * speed * delta);
        y = y + (int)(dY * speed * delta);

        if(x <= width/2 || x >= BreakoutGame.SCREEN_WIDTH - width/2)
            reflectX();
        else if(y >= BreakoutGame.SCREEN_HEIGHT - height/2)
            reflectY();
        else if(y <= height/2)
            game.ballDropped();


        rectangle.setPosition(x, y);
    }

    public void reflectX(){
        dX *= -1;
    }

    public void reflectY(){
        dY *= -1;
    }

    public void reflectPaddle(int paddleX) {
        int distanceFromPaddlecCenter = paddleX - x;
        float normalizedDistance = (float)distanceFromPaddlecCenter/(BreakoutGame.PADDLE_WIDTH/2);
        float bounceAngle = normalizedDistance*MAX_BOUNCE_ANGLE;
        dY = (float)(Math.sin(bounceAngle));
        dX = (float)(-Math.cos(bounceAngle));
    }
}
