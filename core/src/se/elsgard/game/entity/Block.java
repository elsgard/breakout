package se.elsgard.game.entity;

import com.badlogic.gdx.graphics.Texture;

import java.awt.*;

public class Block extends Entity{
    public Block(int width, int height, int x, int y, Texture texture) {
        super(width, height, x, y, texture);
    }
}
