package se.elsgard.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;

import java.awt.*;

public class Paddle extends Entity{

    public Paddle(int width, int height, int x, int y, Texture texture) {
        super(width, height, x, y, texture);
    }

    public void update(){
        x = Gdx.input.getX();
        rectangle.setPosition(x, y);
    }
}
