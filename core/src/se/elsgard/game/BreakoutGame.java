package se.elsgard.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.math.Rectangle;
import se.elsgard.game.entity.Ball;
import se.elsgard.game.entity.Block;
import se.elsgard.game.entity.Entity;
import se.elsgard.game.entity.Paddle;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BreakoutGame extends ApplicationAdapter {
	public static final int SCALE = 2;

	public static final int SCREEN_WIDTH = 480 * SCALE;
	public static final int SCREEN_HEIGHT = 320 * SCALE;

	public static final int BALL_WIDTH = 8 * SCALE;
	public static final int BALL_HEIGHT = 8 * SCALE;

	public static final int PADDLE_WIDTH = 48 * SCALE;
	public static final int PADDLE_HEIGHT = 8 * SCALE;

	public static final int BLOCK_WIDTH = 24 * SCALE;
	public static final int BLOCK_HEIGHT = 12 * SCALE;

	private static int score = 0;
	private static int nBalls = 3;
	private static boolean gameOver = false;

	SpriteBatch batch;
	List<Entity> entities;
	Ball ball;
	Paddle paddle;
	BitmapFont font;
	Texture ballTexture;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
		ballTexture = new Texture("ball.jpg");
		entities = new ArrayList<Entity>();
		font = new BitmapFont();
		setupBlocks();
		setupPaddle();
		setupBall();
	}



	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		if(!gameOver) {
			doCollisions();
			paddle.update();
			updateBall();
			for (Entity e : entities) {
				batch.draw(e.getTexture(), e.getX() - e.getWidth() / 2, e.getY() - e.getHeight(), e.getWidth(), e.getHeight());
			}
			batch.draw(ball.getTexture(), ball.getX() - ball.getWidth(), ball.getY() - ball.getHeight(), ball.getWidth(), ball.getHeight());
			font.draw(batch, String.valueOf(score), 32, 32);
			for (int i = 0; i < nBalls; i++) {
				batch.draw(ballTexture, SCREEN_WIDTH - (BALL_WIDTH * 2 + i * BALL_WIDTH * 2), 32, BALL_WIDTH, BALL_HEIGHT);
			}
		} else {
			font.draw(batch, "GAME OVER", SCREEN_WIDTH/2, SCREEN_HEIGHT/2);
		}
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		ballTexture.dispose();
	}

	private void setupPaddle(){
		paddle = new Paddle(PADDLE_WIDTH, PADDLE_HEIGHT, SCREEN_WIDTH/2, 64, new Texture("paddle.jpg"));
		entities.add(paddle);
	}

	private void setupBall(){
		ball = new Ball(BALL_WIDTH, BALL_HEIGHT, paddle.getX(), paddle.getY() + paddle.getHeight(), new Texture("ball.jpg"), this);
		ball.setAttached(true);
	}

	private void setupBlocks(){
		int rotation = 0;
		for(int y = (SCREEN_HEIGHT/4)*3 + BLOCK_HEIGHT; y <= SCREEN_HEIGHT - BLOCK_HEIGHT; y+=BLOCK_HEIGHT){
			for(int x = BLOCK_WIDTH; x <= SCREEN_WIDTH - BLOCK_WIDTH; x+=BLOCK_WIDTH){
				Texture texture = new Texture("badlogic.jpg");
				if (rotation == 0)
					texture = new Texture("green_block.jpg");
				else if (rotation == 1)
					texture = new Texture("purple_block.jpg");
				else if (rotation == 2) {
					texture = new Texture("red_block.jpg");
				}

				if(rotation == 2)
					rotation = 0;
				else
					rotation++;

				Block block = new Block(BLOCK_WIDTH, BLOCK_HEIGHT, x, y, texture);
				entities.add(block);
			}
		}
	}

    private void updateBall(){
		if(ball.getIsAttached())
			ball.setX(paddle.getX());
		else
			ball.update(Gdx.graphics.getDeltaTime());

		if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)){
			ball.move();
		}
	}

	private void doCollisions(){
    	List<Entity> removeList = new ArrayList<Entity>();
    	if(!ball.getIsAttached() && ball.getShouldCollide()) {
			for (Entity e : entities) {

				double w = 0.5 * (e.getWidth() + ball.getWidth());
				double h = 0.5 * (e.getHeight() + ball.getHeight());

				Vector2 eCenter = new Vector2(e.getX(), e.getY());
				Vector2 ballCenter = new Vector2(ball.getX(), ball.getY());

				double dx = eCenter.x - ballCenter.x;
				double dy = eCenter.y - ballCenter.y;

				if (Math.abs(dx) <= w && Math.abs(dy) <= h) {
					double hx = h * dx;
					double wy = w * dy;

					if (wy > hx)
						if (wy > -hx) {
							ball.reflectY();

						}
						else {
							ball.reflectX();
						}
					else {
						if (wy > -hx) {
							ball.reflectX();
						} else {
								ball.reflectY();
						}
					}
					if (e instanceof Block) {
						score += 50;
						removeList.add(e);
					}
				}
			}
			entities.removeAll(removeList);
		}

	}

	public void ballDropped() {
		nBalls--;
		if(nBalls <= 0) {
			gameOver = true;
		} else {
			setupBall();
		}
	}
}
