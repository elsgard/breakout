package se.elsgard.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import se.elsgard.game.BreakoutGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Breakout";
		config.useGL30 = true;
		config.height = BreakoutGame.SCREEN_HEIGHT;
		config.width = BreakoutGame.SCREEN_WIDTH;
		new LwjglApplication(new BreakoutGame(), config);
	}
}
